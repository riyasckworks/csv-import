<?php

// namespace App\Models;
namespace App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    use HasFactory;
    protected $table = 'uploads';
    protected $fillable = ['module_code', 'module_name', 'module_term'];
}
