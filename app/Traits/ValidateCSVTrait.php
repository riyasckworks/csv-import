<?php

namespace App\Traits;
use Illuminate\Validation\Factory as ValidationFactory;
use Exception;

trait ValidateCSVTrait {
    private $validator;
    private $rules = [
        'Module_code_column'  => 'required',
        'Module_name_column'  => 'required',
        'Module_term_column'  => 'required',
        'Module_code'         => 'required',
        'Module_name'         => 'required',
        'Module_term'         => 'required'
    ];

    public function __construct(ValidationFactory $validator)
    {
        $this->validator = $validator;
    }

    public function validateCSV($csv_file_path) 
    {

        ini_set('auto_detect_line_endings', true);
        if (fopen($csv_file_path, 'r') === false) {
            throw new Exception('File cannot be opened for reading');
        }
        $opened_file = fopen($csv_file_path, 'r');
        $header = fgetcsv($opened_file, 0, ',');

        $Module_code_column = $this->getColumnNameByValue($header, 'Module code');

        $Module_name_column = $this->getColumnNameByValue($header, 'Module name');

        $Module_term_column = $this->getColumnNameByValue($header, 'Module term');

        $data_row = fgetcsv($opened_file, 0, ',');


        $first_row = array_combine($header, $data_row);


        $first_row_module_code = array_key_exists('Module code', $first_row)? $first_row['Module_code'] : '';

        $first_row_module_name = array_key_exists('Module name', $first_row)? $first_row['Module_name'] : '';

        $first_row_module_term = array_key_exists('Module term', $first_row)? $first_row['Module_term'] : '';

        fclose($opened_file);

        $validation_array = [
            'Module_code_column' => $Module_code_column,
            'Module_name_column' => $Module_name_column,
            'Module_term_column' => $Module_term_column,
            'Module_code' => $first_row_module_code,
            'Module_name' => $first_row_module_name,
            'Module_term' => $first_row_module_term
        ];

        return $this->validator->make($validation_array, $this->rules);
    }

    private function getColumnNameByValue($array, $value) 
    {
        return in_array($value, $array)? $value : '';
    }

}
