<?php

namespace App\Http\Controllers;
use App\Models\ImportData;
use Illuminate\Http\Request;
use Validator,Redirect,Response,File,Log;
use App\Jobs\ImportCsv;
use App\Traits\ValidateCSVTrait;

class UploadController extends Controller
{
   
    use  ValidateCSVTrait;
    public function store(Request $request)
    {
       Log::info('initialize store function.');
       $validator = Validator::make($request->all(), 
              [ 
              'file' => 'required|mimes:doc,docx,pdf,txt,csv|max:2048',
        ]);
        if ($validator->fails()) {          
                return response()->json(['error'=>$validator->errors()], 401);                        
        }
        
        if ($files = $request->file('file')) {
             
            //store file into document folder
            // $file = $request->file->store('public/documents');


            // $path = $request->file('file')->getRealPath();
            // $data = array_map('str_getcsv', file($path));
            // $csv_data = array_slice($data, 0, 12);
 
            //store your file into database
            // $upload = new Upload();
            // $upload->module_code = $file;
            // $upload->module_name = $file;
            // $upload->module_term = $file;
            // $upload->save();




            $file = $request->file('file');
            $file->move(base_path('resources/pendingImport'), $file->getClientOriginalName());
            Log::info('moved upload files to pendingImport folder');
             $path = base_path('resources/pendingImport/'.$file->getClientOriginalName());
             $validateMessage =$this->validateCSV($path);
             if ($validateMessage->fails()) {          
                return response()->json(['error'=>$validateMessage->errors()], 401);                        
        }
             print_r($validateMessage->errors());die;
            // $file = file($path);
            // $data = array_slice($file, 1);
            
                  

            
            // $parts = (array_chunk($data, 1000));
            // $i = 1;
            // foreach($parts as $line) {
            //     $filename = base_path('resources/pendingImport/'.date('y-m-d-H-i-s').$i.'.csv');
            //     file_put_contents($filename, $line);
            //     $i++;
            // }
             
            dispatch(new ImportCsv());
            Log::info('Assigned new  job');
              
            return response()->json([
                "success" => true,
                "message" => "Your file has been uploaded! You will receive an email when processing is complete!",
            ]);
  
        }
    
    
    
    
    
    }
}
