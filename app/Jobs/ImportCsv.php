<?php

namespace App\Jobs;
use App\Models\ImportData;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use App\Traits\ValidateCSVTrait;
class ImportCsv implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ValidateCSVTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('initialize ImportCsv job function.');
  
        $path = base_path("resources/pendingImport/*.csv"); 

        $validateMessage =$this->validateCSV($path);
        if ($validateMessage->fails()) {   
            Mail::to('riyasckworks@gmail.com')->send(new SendMailable($validateMessage->errors()));       
                       
         }
    
         foreach (array_slice(glob($path),1,2) as $file) {
        
            $data = array_map('str_getcsv', file($file));

            foreach($data as $row) {

                ImportData::create([
                    'module_code' => $row[0],
                    'module_name' => $row[1],
                    'module_term' => $row[2],
                    ]);  
                 
            }
            Log::info('deleted processed files from folder.');
            unlink($file);
            Mail::to('riyasckworks@gmail.com')->send(new SendMailable("sucessfully imported "));
            Log::info('import details send to email.');
        }
    }
}
