<?php

namespace App\Console\Commands;
use App\Models\ImportData;
use Illuminate\Console\Command;
use Log;

class ImportCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import Data from  csv files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Import csv quee handler.');
  
        $path = base_path("resources/pendingImport/*.csv"); 
    
         foreach (array_slice(glob($path),0,2) as $file) {
        
            $data = array_map('str_getcsv', file($file));

            foreach($data as $row) {
                $str_arr = $row;
                //  explode (",", $row);
                ImportData::create([
                    'module_code' => $str_arr[0],
                    'module_name' => $str_arr[1],
                    'module_term' => $str_arr[2],
                    ]);  

                //delete the file
                 
            }
            unlink($file);
        }
    }
}
